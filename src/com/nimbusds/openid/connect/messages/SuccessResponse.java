package com.nimbusds.openid.connect.messages;


/**
 * Interface for OpenID Connect response messages indicating success.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2012-05-02)
 */
public interface SuccessResponse extends Response {


}
