/**
 * OpenID Connect request, response and error messages.
 *
 * <p>This package implements all required messages for the following OpenID
 * Connect endpoints:
 *
 * <ul>
 *     <li>Authorisation endpoint.
 *     <li>Token endpoint.
 *     <li>Check ID endpoint.
 *     <li>UserInfo endpoint.
 * </ul>
 *
 * <p>Future versions may add support for the optinal dynamic client 
 * registration endpoint.
 * 
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.openid.connect.messages;
