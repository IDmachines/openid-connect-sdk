package com.nimbusds.openid.connect.messages;


/**
 * Marker interface for OpenID Connect messages.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2012-04-09)
 */
public interface Message {


}
