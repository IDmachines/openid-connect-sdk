/**
 * Common OpenID Connect SDK classes.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.openid.connect;
