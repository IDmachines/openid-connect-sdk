/**
 * Claim interface and implementations used by OpenID Connect.
 *
 * <p>A claim is a piece of information about an entity that a claims provider 
 * asserts about that entity. It has a canonical name and a value that 
 * serialises to a JSON boolean, string, number, array, object or null.
 *
 * <p>All OpenID Connect related claims contained in this package implement
 * {@link com.nimbusds.openid.connect.claims.Claim}.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.openid.connect.claims;
