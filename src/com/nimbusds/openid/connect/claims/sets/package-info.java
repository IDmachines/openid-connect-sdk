/**
 * Claims sets used by OpenID Connect.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2012-05-23)
 */
package com.nimbusds.openid.connect.claims.sets;
