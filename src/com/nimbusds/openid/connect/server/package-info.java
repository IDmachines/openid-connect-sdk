/**
 * Endpoint interfaces for OpenID Connect Identity Provider (server) 
 * implementations.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.openid.connect.server;
